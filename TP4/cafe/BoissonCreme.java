package TD3;

/**
 * Classe permettant l'ajout de creme a  une boisson 
 */
public class BoissonCreme extends DecorateurIngredient {
	
	/**
	 * Constructeur
	 * 
	 *  @param boisson a decorer
	 */
	public BoissonCreme(Boisson boisson) {
		super(0.55, " Creme", boisson); //prix = 0.55 description="Creme"
	}
	
	
}

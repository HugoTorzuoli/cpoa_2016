package TD3;

/**
 * Classe correspondant a une boisson Deca
 * 
 */
public class Deca extends Boisson {
	
	public Deca() {
		description = " Decafeine";
	}

	/**
	 * @return prix de la boisson
	 */
	public double cout() {
		return 1;
	}
	
}

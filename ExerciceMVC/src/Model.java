package exercice_mvc;

import java.util.ArrayList;
import java.util.Observable;

public class Model extends Observable {
	
	private ArrayList<String> listeMots;
	private String petit, grand;
	
	public Model(){
		this.listeMots = new ArrayList<String>();		
		
	}
	
	public void ajouterMot(String mot){
		this.listeMots.add(mot);
		comparer(mot);
				
	}
	
	//compareTo : si 1er plus petit que le 2e, renvoi 1
	//			  si 1er plus grand que le 2e, renvoi -1
	public void comparer(String mot){
		if(mot.compareTo(this.petit)>0)
			this.petit = mot;
		else
		if(mot.compareTo(this.grand)<0)
			this.grand = mot;
	}
	
	
	
	
	

	public String getPetit() {
		return petit;
	}
	public void setPetit(String petit) {
		this.petit = petit;
	}
	public String getGrand() {
		return grand;
	}
	public void setGrand(String grand) {
		this.grand = grand;
	}

	public ArrayList<String> getListeMots() {
		return listeMots;
	}

	public void setListeMots(ArrayList<String> listeMots) {
		this.listeMots = listeMots;
	}
	

}

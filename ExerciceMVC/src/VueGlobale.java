import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class VueGlobale implements Observer{
	
	/**
	 * la liste de mot
	 */
	public JTextArea liste;
	
	/**
	 * le modele
	 */
	public Model modele;
	
	public VueGlobale(){
		this.liste = new JTextArea(5,5);
	}

	@Override
	public void update(Observable o, Object arg) {
				
	}
}

import java.util.*;

/**
 * Classe implementant Iterator, excepte la methode remove
 */
public abstract class Parcours implements Iterator<Integer>{

	private TableauEntier tab;
	private int ligneCour, colonneCour, nbParcourus;

	public Parcours(TableauEntier tab){
		this.tab = tab;
		this.ligneCour = 0;
		this.colonneCour = 0;
		this.nbParcourus = 0;
	}


	@Override
	/**
	 * verifie s'il reste des elements a parcourir dans le tableau
	 * @return [description]
	 */
	public boolean hasNext() {
		return tab.getHauteur()*tab.getLargeur() > nbParcourus;
	}
	

	@Override
	/**
	 * Retourne la valeur suivante dans le tableau par rapport aux elements courants
	 * @return [description]
	 */
	public Integer next(){
		int v = tab.valeurA(ligneCour, colonneCour);
		suivant();
		nbParcourus++;
		return v;
	}

	/**
	 * methode suivant qui sera utilisee dans les classes
	 */
	public abstract void suivant();

		/**
	* Getter et Setter
	*/

	public int getLigneCour() {
		return ligneCour;
	}

	public void setLigneCour(int ligneCour) {
		this.ligneCour = ligneCour;
	}

	public int getNbParcourus() {
		return nbParcourus;
	}

	public void setNbParcourus(int nbParcourus) {
		this.nbParcourus = nbParcourus;
	}

	public int getColonneCour() {
		return colonneCour;
	}

	public void setColonneCour(int colonneCour) {
		this.colonneCour = colonneCour;
	}

	public TableauEntier getTab() {
		return tab;
	}

	public void setTab(TableauEntier tab) {
		this.tab = tab;
	}
}
public class TableauEntier{
	private int[][] tab;
	private int largeur;
	private int hauteur;

	public TableauEntier(int[][] t){
		this.tab = t;
	}

	public int valeurA(int l, int c){
		return this.tab[l][c];
	}

	public int getLargeur(){
		return this.largeur;
	}

	public int getHauteur(){
		return this.hauteur;
	}
}
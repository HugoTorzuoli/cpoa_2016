public class ParcoursLigne extends Parcours{
	
	/**
	 * methode suivant
	 * utilisee dans next
	 * incremente les attributs de Parcours
	 */
	public void suivant(){
		// s'il y a une valeur dans le tableau
		if (this.tab.hasNext()){
			// si la colonne courante est egale a la largeur du tableau (si on est a l'extremite)
			if (this.tab.getLargeur() == this.getColonneCour()){
				// On change de ligne
				this.ligneCour++;
				// On remet a zero la colonne courante
				this.setColonneCour(0);
			}else{
				this.setLigneCour(this.getLigneCour()+1);
			}
		}
	}
}
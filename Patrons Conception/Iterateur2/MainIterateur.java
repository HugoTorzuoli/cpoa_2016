import java.util.Random;

public class MainIterateur{
	/**
	 * Methode main
	 * args[0] : nbColonnes
	 * args[1] : nbLignes
	 * args[2] : borne superieure random
	 */
	public static void main(String[] args){
		int nbColonnes = Integer.parseInt(args[0]);
		int nbLignes = Integer.parseInt(args[1]);

		int[][] tab = new int[nbColonnes][nbLignes];

		Random r = new Random();
		for (int i = 0; i < tab.length ; i++){
			for (int j = 0; j < tab[i].length ; j++){
				tab[i][j] = r.nextInt(Integer.parseInt(args[2]));
			}
		}

		TableauEntier tableauEntier = new TableauEntier(tab);

		ParcoursLigne parcours = new ParcoursLigne(tableauEntier);
	}
}
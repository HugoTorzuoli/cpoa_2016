public class AdaptateurText extends TextDrawer implements Dessinable{

	private String text;

	public AdaptateurText(String txt) {
		text = txt;
	}

	public void dessine() {
		drawText(text);
	}

	public void dessinerTableau(){
	Dessinable[] formes = { new Rectangle(), new Cercle(), new AdaptateurText("bla bla") };
	for(Dessinable d : formes)
		d.dessine();
	}

}
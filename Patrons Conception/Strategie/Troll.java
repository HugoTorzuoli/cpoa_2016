public class Troll extends Personnage{

	public Troll(){
		tech = new TechniqueCombat();
	}

	public void armer(TechniqueCombat t){
		tech = t;
	}

	public void combattre(){
		tech.utiliserArme();
	}
}
public abstract class Personnage {

	protected TechniqueCombat tech;

	public void armer(TechniqueCombat t);

	public void combattre();
}
public class AdresseIp implements Comparable<AdresseIp>{

	String numIp;

	public AdresseIp(String s){
		this.numIp = s;
	}

	public int compareTo(AdresseIp ipParam){
		long n1 = this.numero();
		long n2 = ipParam.numero();

		if (n1>n2)
			return 1;
		else
			if (n1<n2)
				return -1;
			else
				return 0;
	}

	private long numero(){
		String[] num = this.numIp.split("\\.");

		if (num.length!=4)
			return (-1);

		long res = 0;

		for (int i = 0; i < 4 ; i++)
		{
			int digit = Integer.parseInt(num[i]);
			res += (long) (Math.pow(1000,3-i))*digit;
		}

		return res;
	}
}
import java.io.*;
import java.util.*;

import javax.swing.text.html.HTMLDocument.Iterator;

public class ListIp{
	private Set<AdresseIp> ips;

	/**
	 * Si tri est vrai, on tri les IP par ordre alphabetique
	 * @param  tri [description]
	 * @return     [description]
	 */
	public ListIp(boolean tri){
		if (tri)
			this.ips = new TreeSet<AdresseIp>();
		else
			this.ips = new HashSet<AdresseIp>();
	}

	public void chargerFichier(String name){
		// Lire le fichier logs.txt
		try{
			InputStream is = new FileInputStream(name);
			InputStreamReader isReader = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isReader);
			String ligne = br.readLine();
			while(ligne!=null){
				String[] decoup = ligne.split(" ");
				this.ips.add(decoup[0]);
				ligne = br.readLine();
			}
			br.close();
		}catch(Exception e){
			System.out.println(e.toString());
		}
	}

	public String toString(){
		String res = "";
		java.util.Iterator<String> it = this.ips.iterator();
		while(it.hasNext()){
			res += it.next() + "\n";
		}

		return res;
	}

	public static void main(String[] args){
		ListIp list = new ListIp(true);
		list.chargerFichier(args[0]);
		System.out.println(list.toString());
	}

}
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Etudiant{
	private Identite identite;
	private HashMap<String,ArrayList<Float>> lNotes;
	private Formation formation;

	public Etudiant(Identite id, Formation f){
		this.identite = id;
		this.formation = f;
		this.lNotes = new HashMap<String,ArrayList<Float>>();
	}

	@Override
	public String toString() {
		return "Etudiant [identite=" + identite + ", lNotes=" + lNotes
				+ ", formation=" + formation + "]";
	}
	/**
	 * methode ajouterNote
	 * permet d'ajouter une note a un etudiant pour une certaine matiere
	 * @param matiere
	 * matiere de la note ajoutee
	 * @param note
	 * note a ajouter
	 * @return
	 * boolean indiquant si la note a ete ajoute ou non
	 */
	/*
	 public boolean ajouterNote(String matiere, float note){
		//Si la note est correcte
		if (note >= 0 && note <= 20){
			// On recup les anciennes notes associees a cette matiere
			Collection<Float> notes = lNotes.get(matiere);
			// Si la matiere n'existe pas
			if (notes==null){
				return false;
			}else{
				// Sinon on ajoute la note
				notes.add(note);
				lNotes = (HashMap<String, ArrayList<Float>>) notes;
				return true;
			}
		}else{
			return false;
		}
	}
	*/
	
	public void ajouterNote(String matiere, float note) throws AjouterNoteException2, AjouterNoteException{
		//Si la note est correcte
		if(note >= 0 && note <= 20){
			//recup des anciennes notes associees a la matiere
			Collection<Float> notes = lNotes.get(matiere);
			// si la matiere n'existe pas
			if(notes==null)
				throw new AjouterNoteException2();
			else{
				notes.add(note);
				lNotes = (HashMap<String, ArrayList<Float>>) notes;
			}
		}
		else
			throw new AjouterNoteException();
	}

	/**
	 * methode calculerMoyenne
	 * calcule la moyenne d'une matiere donnee
	 * @param m
	 * matiere de la moyenne a calculer
	 * @return moyenne de la matiere
	 * @throws AjouterNoteException2
	 */
	public float calculerMoyenne(String m) throws AjouterNoteException2 {
		float result = 0;
		ArrayList<Float> tmp = new ArrayList<Float>();
		if(this.lNotes.containsKey(m)){
			tmp = lNotes.get(m);
			for(int i = 0; i < tmp.size() ; i++){
				result += tmp.get(i);
			}
			result /= tmp.size();
		}
		else
			throw new AjouterNoteException2();
		return result;

	}
	/**
	 * methode calculerMoyenneGenerale
	 * calcule la moyenne général de l'etudiant en tenant compte des coefficients
	 * @return moyenne generale de l'etudiant
	 * @throws AjouterNoteException2
	 */
	public float calculerMoyenneGenerale() throws AjouterNoteException2 {
		float result = 0;
		int i = 0; //somme des coefficients de chaque matiere pour le resultat final
		Set<String> clefs = this.lNotes.keySet();
		for(String s : clefs){
			result += calculerMoyenne(s);
			result *= this.formation.getCoefficient(s);
			i += this.formation.getCoefficient(s);
		}
		result /= this.lNotes.size() + i;
		return result;
	}
	
	/**
	 * getter formation
	 * @return
	 */
	public Formation getFormation(){
		return this.formation;
	}
	
	/**
	 * getter identite
	 * @return
	 */
	public Identite getIdentite(){
		return this.identite;
	}
}
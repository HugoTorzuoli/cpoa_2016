/**
 * Classe identite
 */
public class Identite{
	/**
	 * Numero d'indentification
	 */
	private String NIP;

	/**
	 * nom
	 */
	private String nom;

	/**
	 * prenom
	 */
	private String prenom;

	public String getNIP(){
		return this.NIP;
	}

	public String getNom(){
		return this.nom;
	}

	public String getPrenom(){
		return this.prenom;
	}

	public void setNIP(String nip){
		this.NIP = nip;
	}

	public void setNom(String n){
		this.nom = n;
	}

	public void setPrenom(String p){
		this.prenom = p;
	}

	/**
	 * Constructeur
	 * @param  nip [description]
	 * @param  n   [description]
	 * @param  p   [description]
	 */
	public Identite(String nip, String n, String p){
		this.NIP = nip;
		this.nom = n;
		this.prenom = p;
	}

	public String toString(){
		return ("Numero d'identification : " + this.NIP + ", Nom : " + this.nom + ", Prenom : " + this.prenom);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((NIP == null) ? 0 : NIP.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identite other = (Identite) obj;
		if (NIP == null) {
			if (other.NIP != null)
				return false;
		} else if (!NIP.equals(other.NIP))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

	

}

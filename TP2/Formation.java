import java.util.HashMap;

/**
 * Classe formation
 * 	la formation que suit l'etudiant
 */
public class Formation{
	/**
	 * l'id de la formation
	 */
	private String id;

	/**
	 * la collection des matieres
	 * 	le nom de la matiere - son coefficient
	 */
	private HashMap<String, Integer> lMatieres = new HashMap<String, Integer>();

	public Formation(String id, HashMap<String,Integer> map){
		this.id = id;
		this.lMatieres = map;
	}

	@Override
	public String toString() {
		return "Formation [id=" + this.id + ", lMatieres=" + this.lMatieres + "]";
	}

	/**
	 * [ajouterMatiere]
	 * 	permet d'ajouter une matiere si celle-ci n'est pa deja dans la formation
	 * @param  matiere [description]
	 * @param  coeff   [description]
	 * @return vrai si ajout reussi
	 */
	public boolean ajouterMatiere(String matiere, int coeff){
		if (this.lMatieres.containsKey(matiere)){
			return false;
		}
		else{
			this.lMatieres.put(matiere, coeff);
			return true;
		}
	}

	/**
	 * [supprimerMatiere]
	 * supprimer une matiere si celle-ci existe
	 * @param  matiere [description]
	 * @return vrai si suppression reussie
	 */
	public boolean supprimerMatiere(String matiere){
		if (this.lMatieres.containsKey(matiere)){
			this.lMatieres.remove(matiere);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * [getCoefficient]
	 * @param matiere
	 * matiere du coefficient recherche
	 * @return coefficient de la matiere matiere
	 * @throws AjouterNoteException2
	 */
	public int getCoefficient(String matiere) throws AjouterNoteException2{
		if (this.lMatieres.containsKey(matiere)){
			return this.lMatieres.get(matiere);
		}else{
			throw new AjouterNoteException2();
		}
	}
	
	/**
	 * getter id
	 * @return
	 */
	public String getId(){
		return this.id;
	}
}
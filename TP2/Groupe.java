import java.util.*;

public class Groupe {

	private Formation formation;
	private ArrayList<Etudiant> etudiants;
	
	
	public Groupe(Formation form){
		this.etudiants = new ArrayList<Etudiant>();
		this.formation = form;
	}
	
	public void ajoutEtudiant(Etudiant nom) {
		//on verifie l'id de la formation de l'etudiant rentre en param et de la formation du groupe
		if(nom.getFormation().getId().equals(this.formation.getId()))
			this.etudiants.add(nom);		
	}

	public void supprimerEtudiant(Etudiant nom){
		if(this.etudiants.contains(nom))
			this.etudiants.remove(nom);
	}
	
	public double moyenneMatiere(String matiere) throws AjouterNoteException2{
		double res = 0;
		int count = 0;
		
		if (this.formation.getLMatieres().containsKey(matiere)){
			for(Etudiant iter: this.etudiants){
				res += iter.calculerMoyenneGenerale();
				count++;
			}
		}
		
		return res/count;
	}

	public double moyenneGen(){

	}

	public void triParMerite(){
		Collections.sort(etudiants, new ElementComparatorM());
		System.out.println("Tri par merite \n"+etudiants);


	}


	public void triAlpha(){
		Collections.sort(etudiants, new ElementComparatorA());
		System.out.println("Tri alphabetique \n" + etudiants);
	}
	
}
